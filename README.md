# Building Oracle database docker image

Oracle does not provide a ready-to-use docker image; instead it requires that
users build their own images. This requires access to Oracle support to download
Oracle database distribution archive.

To build Oracle SE2 docker image:

```
git clone https://github.com/oracle/docker-images.git
cp -r docker-images/OracleDatabase/SingleInstance/dockerfiles/19.3.0 .
cd 19.3.0
wget https://download.oracle.com/otn/linux/oracle19c/190000/LINUX.X64_193000_db_home.zip
docker build -t oracle/database:19.3.0-standard --build-arg DB_EDITION=SE2 .
```

Starting Oracle first time can be time-consuming as RDBMS will be creating database files.
To run Oracle database server:

```
docker run -d -p 1521:1521 -p 5500:5500 -it \
-e ORACLE_SID=orcl \
-e ORACLE_PDB=pdb01 \
-e ORACLE_PWD=password \
-e ORACLE_EDITION=SE2 \
-v /your/oracle/dir:/opt/oracle/oradata \
--name oracle oracle/database:19.3.0-standard
```

Optional parameters:

```
-e ORACLE_CHARACTERSET=<nsi charset> \
-e INIT_SGA_SIZE=<your database SGA memory in MB> \
-e INIT_PGA_SIZE=<your database PGA memory in MB> \
```

## sqlplus

To install a Mac client with `sqlplus` (for more details refer to https://vanwollingen.nl/install-oracle-instant-client-and-sqlplus-using-homebrew-a233ce224bf):

Download the two files from http://www.oracle.com/technetwork/topics/intel-macsoft-096467.html:

- instantclient-basic-macos.x64–<version>.zip
- instantclient-sqlplus-macos.x64–<version>.zip

Put the files into `~/Library/Caches/Homebrew` then use `brew` to install:

```
brew tap InstantClientTap/instantclient
brew install instantclient-basic
brew install instantclient-sqlplus
```

To connect to Oracle docker instance:

```
sqlplus sys/password@//localhost:1521/pdb01 as sysdba
```
